import request from '@/utils/request'

export function list(query) {
  return request({
    url: '/kitlog/list',
    method: 'get',
    params: query
  })
}

export function get(id) {
  return request({
    url: `/kitlog/${id}`,
    method: 'get'
  })
}

export function del(id) {
  return request({
    url: `/kitlog/${id}`,
    method: 'delete'
  })
}
