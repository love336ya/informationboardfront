import request from '@/utils/request'

export function list(query) {
  return request({
    url: '/device/list',
    method: 'get',
    params: query
  })
}
export function postList(query) {
  return request({
    url: '/device/post/list',
    method: 'get',
    params: query
  })
}

export function get(id) {
  return request({
    url: `/device/${id}`,
    method: 'get'
  })
}

export function add(data) {
  return request({
    url: '/device',
    method: 'post',
    data: data
  })
}

export function update(data) {
  return request({
    url: '/device',
    method: 'put',
    data: data
  })
}

export function del(id) {
  return request({
    url: `/device/${id}`,
    method: 'delete'
  })
}

export function post(data) {
  return request({
    url: '/device/post',
    method: 'post',
    data: data
  })
}

export function status(id) {
  return request({
    url: `/device/status/${id}`,
    method: 'get'
  })
}

export function content(id) {
  return request({
    url: `/device/data/${id}`,
    method: 'get'
  })
}

export function tasks(query) {
  return request({
    url: '/device/task/list',
    method: 'get',
    params: query
  })
}

export function getDatas(data) {
  return request({
    url: '/device/getDatas',
    method: 'post',
    data: data
  })
}
