import request from '@/utils/request'

export function list(query) {
  return request({
    url: '/device/post/list',
    method: 'get',
    params: query
  })
}

export function get(id) {
  return request({
    url: `/device/post/${id}`,
    method: 'get'
  })
}

export function del(id) {
  return request({
    url: `/device/post/${id}`,
    method: 'delete'
  })
}
