import request from '@/utils/request'

export function list(query) {
  return request({
    url: '/model/list',
    method: 'get',
    params: query
  })
}

export function get(id) {
  return request({
    url: `/model/${id}`,
    method: 'get'
  })
}

export function add(data) {
  return request({
    url: '/model',
    method: 'post',
    data: data
  })
}

export function update(data) {
  return request({
    url: '/model',
    method: 'put',
    data: data
  })
}

export function del(id) {
  return request({
    url: `/model/${id}`,
    method: 'delete'
  })
}
