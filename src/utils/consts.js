export const TYPE_LIST = [
  {
    label: "情报板",
    value: 1
  },
  {
    label: "气象站",
    value: 2
  },
  {
    label: "车感器",
    value: 3
  }
];

export const BRAND_LIST = [
  {
    label: "三思",
    value: 1
  },
  {
    label: "电明",
    value: 2
  },
  {
    label: "金晓",
    value: 3
  },
  {
    label: "诺瓦",
    value: 4
  }
];
